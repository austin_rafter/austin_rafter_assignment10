package questionTwo;
/*
 *Austin Rafter
 * CS 049J
 *
 * double that extends MeasureableTwo generic class
 */
public class DoubleTwo extends MeasureableTwo<Double> {
    double doubleRecieved;

    //constructor to take in double
    public DoubleTwo(double doubleEntered){
        doubleRecieved = doubleEntered;
    }
    //returns double
    public double getMeasure(){
        return doubleRecieved;
    }

    //returns String so object is not printed
    @Override
    public String toString(){
        return "" + doubleRecieved;
    }


}
