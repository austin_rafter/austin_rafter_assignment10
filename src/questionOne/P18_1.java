package questionOne;
/*
 *Austin Rafter
 * CS 049J
 *
 * passes two measurable ints
 * and two measurable doubles
 * into an array and print the pair of highest and lowest
 */
public class P18_1 {

    public static void main(String[] args) {

        PairUtil.Measurable[] measurableArray = new PairUtil.Measurable[4];

        Integer intOne = new Integer(3);
        Integer intTwo = new Integer(1);
        Double doubleOne = new Double(5.5);
        Double doubleTwo = new Double(99.0);


        measurableArray[0] = intOne;
        measurableArray[1] = doubleOne;
        measurableArray[2] = doubleTwo;
        measurableArray[3] = intTwo;

        System.out.println("(" + PairUtil.minMax(measurableArray).getFirst() + "," + PairUtil.minMax(measurableArray).getSecond() + ")");





    }





}
