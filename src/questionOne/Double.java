package questionOne;
/*
 *Austin Rafter
 * CS 049J
 *
 *double that implements usable interface
 */
public class Double implements PairUtil.Measurable{

    double doubleRecieved;

    //constructor to set double
    public Double(double doubleEntered){
        doubleRecieved = doubleEntered;
    }
    //return double
    public double getMeasure(){
        return doubleRecieved;
    }
}
