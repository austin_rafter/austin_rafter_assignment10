package questionTwo;

import java.util.ArrayList;

public abstract class MeasureableTwo <T> {

    public MeasureableTwo(){}

    abstract double getMeasure();

    public abstract String toString();

    /*
    Search through ArrayList of MeasureableTwo objects
    return the greatest
     */
    public static <T extends MeasureableTwo> T greatestOfArrayList(ArrayList<T> objects){
        T greatest = objects.get(0);
        for (int i = 1; i < objects.size(); i++) {
            T obj = objects.get(i);
            if (obj.getMeasure() > greatest.getMeasure()) {
                greatest = obj;
            }
        }
        return greatest;
    }
}
