package questionThree;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//import questionThree.WordCount;
/*
 *Austin Rafter
 * CS 049J
 *
 *reads through files setting them into threads
 * starting the threads
 * then printing the word count and total  word count
 */
public class E22_7 {

    public static void main(String[] args) {


        /*
       loop through args setting each file into WordCount object
       create thread for each runWordCount and start each thread
         */
        for(int i = 0; i < args.length; i++){
            WordCount runWordCount = new WordCount(args[i]);
            Thread wordCount = new Thread(runWordCount);
            wordCount.start();
            //System.out.println(args[i]);
        }

        /*
        //set file names into a string
        String strFileNames = "src/questionThree/words.txt src/questionThree/otherWords.txt src/questionOne/Integer.java src/questionOne/Double.java src/questionTwo/IntegerTwo.java";

        //split string at spaces
        String[] Files = strFileNames.split(" ");

        /*
        loop through the String array of files
        set each file into a WordCount object
        create thread for each object
        start each thread

        for(String file : Files){
        WordCount runWordCount = new WordCount(file);
        Thread wordCount = new Thread(runWordCount);
        wordCount.start();
        }
         */

    }

    /*
     * takes in fileName and reads through the file counting words
     *
     * Thread.activeCount found at
     * https://www.tutorialspoint.com/java/lang/thread_activecount.htm
     *
     */

    public static class WordCount implements Runnable{

        private Lock fileChangeLock;
        private Condition reachedEndOfFileCondition;
        static int nTotalWordCount = 0;
        static int nActiveThreads = 0;
        String fileName;
        private static final int DELAY = 1000;

        //constructor to set file name, lock, and Condition
        public WordCount(String fileNameEntered){
            fileName = fileNameEntered;
            fileChangeLock = new ReentrantLock();
            reachedEndOfFileCondition = fileChangeLock.newCondition();
        }



        public void run() {
            //lock thread
            fileChangeLock.lock();
            try {
                int nFileWordCount = 0;
                File fileNameRecieved = new File("text");
                Scanner scanFile = new Scanner(System.in);
                nActiveThreads++;
                try{
                    //read file name and scan
                    fileNameRecieved = new File(fileName);
                    scanFile = new Scanner(fileNameRecieved);
                }catch(FileNotFoundException fileNotFound){
                    System.out.println("File not found");
                }
                scanFile.useDelimiter("[^A-Za-z]+");
                //loop through file adding 1 to nFileWordCount for each word in the file
                while(scanFile.hasNext()){
                    scanFile.next();
                    nFileWordCount++;
                }
                //delay for one second after each file
                try{
                    Thread.sleep(DELAY);
                }catch(InterruptedException exception){

                }
                scanFile.close();
                //print
                System.out.println(fileName + ": " + nFileWordCount);
                //add number of words in file to total word count
                //System.out.println(Thread.activeCount());
                nTotalWordCount+=nFileWordCount;
                //System.out.println(Thread.activeCount());
                int threadCount = Thread.activeCount() - 1;
                nActiveThreads--;
                //System.out.println(nActiveThreads);
                //System.out.println(nActiveThreads);
                //if active thread count is equal to nActiveThreads then print total word count
                if(((0) == nActiveThreads)){
                    System.out.println("Total word count: " + nTotalWordCount);
                }

            } finally {
                fileChangeLock.unlock();
            }
        }

    }

}
