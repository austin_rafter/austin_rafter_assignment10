package questionOne;
/*
 *Austin Rafter
 * CS 049J
 *
 *takes in array of measurable objects
 * goes through and sets minimum and maximum numbers into pair
 *
 * usable interface has a method that returns the double of a number
 */
public class PairUtil  {

    public static <T extends Measurable> Pair<T,T> minMax(T[] objects){

        //set first object into smallest and greatest
        T smallest = objects[0];
        T greatest = objects[0];
        //loop through objects replacing smallest if the next iterations
        //object is smaller than current smallest
        for (int i = 1; i < objects.length; i++)
        {
            T obj = objects[i];
            if (obj.getMeasure() < smallest.getMeasure())
            {
                smallest = obj;
            }
        }
        //loop through objects replacing greatest if the next iterations
        //object is greater than current greatest
        for (int i = 1; i < objects.length; i++)
        {
            T obj = objects[i];
            if (obj.getMeasure() > greatest.getMeasure())
            {
                greatest =  obj;
            }
        }
        //set smallest and greatest in double format into pair
        Pair pair = new Pair(smallest.getMeasure(),greatest.getMeasure());
        //return the pair
        return pair;
    }


    public interface Measurable<T>
    {
        /*
         Computes the measure of the object.
         @return the measure as a double
         */
        double getMeasure();
    }
}
