package questionOne;

/*
 *Austin Rafter
 * CS 049J
 *
 * Integer that implements Measurable interface
 */
public class Integer implements PairUtil.Measurable {
    int integer;

    //constructor to set int
    public Integer(int intEntered){
        integer = intEntered;
    }
    //return int as double
    public double getMeasure(){
        return integer;
    }
}
