package questionTwo;

/*
 *Austin Rafter
 * CS 049J
 *
 * adds 3 MeasureableTwo Ints and Doubles
 * to an ArrayList then prints the largest
 */

import java.util.ArrayList;

public class P18_4 {
    public static void main(String[] args) {
        ArrayList<MeasureableTwo> measureableArrrayList = new ArrayList<>();

        IntegerTwo intOne = new IntegerTwo(3);
        IntegerTwo intTwo = new IntegerTwo(6);
        IntegerTwo intThree = new IntegerTwo(37);
        DoubleTwo doubleOne = new DoubleTwo(23.5);
        DoubleTwo doubleTwo = new DoubleTwo(20.5);

        measureableArrrayList.add(intOne);
        measureableArrrayList.add(intTwo);
        measureableArrrayList.add(intThree);
        measureableArrrayList.add(doubleOne);
        measureableArrrayList.add(doubleTwo);

        System.out.print("Largest element: " + MeasureableTwo.greatestOfArrayList(measureableArrrayList).toString());
    }
}
