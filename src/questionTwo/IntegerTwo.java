package questionTwo;
/*
 *Austin Rafter
 * CS 049J
 *
 *Integer that implements MeasureableTwo class
 */
public class IntegerTwo extends MeasureableTwo<Integer>{
    int integer;
    ///constructor to take int
    public IntegerTwo(int intEntered){
        integer = intEntered;
    }

    //returns int as double
    public double getMeasure(){
        return (int) integer;
    }

    //returns String so object is not printed
    public String toString(){
        return "" + integer;
    }



}
